package automation.common.calc

class Calculator {

    companion object {
        @JvmStatic
        fun sum(a: Int, b: Int): Int {
            return a + b
        }

        @JvmStatic
        fun substract(a: Int, b: Int): Int {
            return a - b
        }

        @JvmStatic
        fun multiply(a: Int, b: Int): Int {
            return a * b
        }

        @JvmStatic
        fun divide(a: Double, b: Double): Double {
            return a / b
        }
    }
}