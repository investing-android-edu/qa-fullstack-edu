package kotlin_lang.homework1

/**
 * Домашнее задание №1. Информация в README.md файле
 */
fun main(args:Array<String>) {
    val arr = arrayOf(1, 4, 3, -9, 0, 16, 29, 21, 6, 45, 9)

    val sortArray = HomeWork1.sortArray(arr)
    HomeWork1.printArray(sortArray)

    val min = HomeWork1.findMin(arr)
    println("Минимальный элемент массива = $min")

    val max = HomeWork1.findMax(arr)
    println("Максимальный элемент массива = $max")
}

class HomeWork1 {

    companion object {
        @JvmStatic
        fun sortArray(arr: Array<Int>): Array<Int> {
            // TODO: Реализовать метод
            return arrayOf()
        }

        fun findMin(arr: Array<Int>): Int {
            // TODO: Реализовать метод
            return 0
        }

        fun findMax(arr: Array<Int>): Int {
            // TODO: Реализовать метод
            return 0
        }

        fun printArray(arr: Array<Int>) {
            arr.forEach { print("$it ") }
            println()
        }
    }
}