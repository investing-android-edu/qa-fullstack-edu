package kotlin_lang.homework3

import java.lang.Exception
import java.lang.IllegalArgumentException
import java.lang.RuntimeException

fun main(args: Array<String>) {
    try {
        foo(1)  // Здесь можно передавать разные числа, чтобы проверить правильно построенную иерархию исключений
    } catch (e: Exception) {
        print("Поймано исключение 'Exception'")
    } // TODO: Добавьте блоки с исключениями, не забудьте их правильно выстроить
}

fun foo(number: Int) {
    when (number) {
        1 -> {
            println("Выбрасывается исключение 'Exception'")
            throw Exception()
        }
        2 -> {
            println("Выбрасывается исключение 'RuntimeException'")
            throw RuntimeException()
        }
        3 -> {
            println("Выбрасывается исключение 'IllegalArgumentException'")
            throw IllegalArgumentException()
        }
    }
    print("Ты передал не то число, дружочек!")
}